package edu.iit.cs445.delectable.Boundaries;

import java.util.List;

import edu.iit.cs445.delectable.menuItem;

public interface MenuBoundaryInterface {
	
	List<menuItem> getMenu();
	menuItem getMenuItem(int foodID);

}
