package edu.iit.cs445.delectable;

//Customer creation is to be handled by the UI/UX FrontEnd

public class Customer{

	private int id = -1;
	private String name;
	private String email;
	private String phone;
	private Address shippingAddress;
	
//CONSTRUCTORS
	public Customer(){}
	
	public Customer(String name, String email, String phoneNumber, Address shippingAddress){
		this.name = name;
		this.email = email;
		this.phone = phoneNumber;
		this.shippingAddress = shippingAddress;
		setID();
	}
	
//ACCESSORS
	public int getID(){
		return this.id;
	}
	public String getName(){
		return this.name;
	}
	public String getEmail(){
		return this.email;
	}
	public String getPhoneNumber(){
		return this.phone;
	}
	public Address getShippingAddress(){
		return this.shippingAddress;
	}
	
//MUTATORS
	public void setName(String name){
		this.name = name;
	}
	public void setEmail(String email){
		this.email = email;
	}
	public void setPhoneNumber(String phoneNumber){
		this.phone = phoneNumber;
	}
	public void setShippingAddress(Address shippingAddress){
		this.shippingAddress = shippingAddress;
	}
	
//METHODS
	public boolean equals(Customer testCustomer){
		if (this.name.equals(testCustomer.getName()) && this.email.equals(testCustomer.getEmail()) && this.phone.equals(testCustomer.getPhoneNumber()) && this.shippingAddress.equals(testCustomer.getShippingAddress())){
			return true;
		} else {
			return false;
		}
	}
	
	public void setID(){
		for (int i = 0; i < orderList.list.size(); i++){
			Order testOrder = orderList.list.get(i);
			Customer testCustomer = testOrder.personal_info;
			if (testCustomer.email.equals(this.email)){
				this.id = testCustomer.id;
			}
		}
		if (this.id == -1){
			this.id = UniqueIdGenerator.getUniqueID();
		}
	}
}
