package edu.iit.cs445.delectable.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.iit.cs445.delectable.Address;
import edu.iit.cs445.delectable.Customer;

public class testCustomer {

	Address testAddress = new Address("testCity", "testState", "testZip", "testStreet");
	Customer testCustomer = new Customer("Patrick M", "mendus59@gmail.com", "7249611117", testAddress);

	@Test
	public void testEqualsCustomer() {
		Customer testCustomer2 = new Customer("Patrick M", "mendus59@gmail.com", "7249611117", testAddress);
		assertTrue(testCustomer.equals(testCustomer2));
	}

	@Test
	public void testSetID() {
		Customer testCustomer3 = new Customer();
		assertTrue(testCustomer3.getID() == -1);
		testCustomer3.setID();
		assertFalse(testCustomer3.getID() == -1);
	}

}
