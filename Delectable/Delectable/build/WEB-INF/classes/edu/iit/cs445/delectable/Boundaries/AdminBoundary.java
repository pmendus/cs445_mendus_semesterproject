package edu.iit.cs445.delectable.Boundaries;

import com.google.gson.JsonObject;

import edu.iit.cs445.delectable.Menu;
import edu.iit.cs445.delectable.Order;
import edu.iit.cs445.delectable.UniqueIdGenerator;
import edu.iit.cs445.delectable.dateHandler;
import edu.iit.cs445.delectable.menuItem;
import edu.iit.cs445.delectable.orderList;
import edu.iit.cs445.delectable.surchargeHandler;

public class AdminBoundary implements AdminBoundaryInterface{

	public AdminBoundary(){}
	
	public surchargeHandler getSurchargeHandler(){
		surchargeHandler sh = new surchargeHandler();
		return sh;
	}
	
	public void setSurcharge(double surcharge){
		surchargeHandler.setSurcharge(surcharge);
	}
	
	public void createMenuItem(menuItem newItem){
		newItem.foodID = UniqueIdGenerator.getUniqueID();
		newItem.create_date = dateHandler.getCurrentDate();
		newItem.last_modified_date = dateHandler.getCurrentDate();
		Menu.DelectableMenu.addMenuItem(newItem);
	}
	
	public void setMenuItemPrice(int menuItemID, double price){
		Menu.DelectableMenu.getMenuItem(menuItemID).setPrice(price);
	}
	
	public JsonObject putMenuItemResponse(menuItem item){
		JsonObject returnObject = new JsonObject();
		returnObject.addProperty("id", item.foodID);
		return returnObject;
	}
	
	public Order getOrder(int orderID){
		Order returnOrder = orderList.DelectableOrders.getOrder(orderID);
		return returnOrder;
	}
	
	public void deliverOrder(int orderID){
		for (int i = 0; i < orderList.list.size(); i++){
			Order removeOrder = orderList.list.get(i);
			if (removeOrder.orderID == orderID){
				orderList.list.get(i).status = "delivered";
				return;
			}
		}
	}
	/*
	public void setMenuItemPrice(int menuItemID, double price){
		for (int i = 0; i < Menu.menu.size(); i++){
			if (Menu.menu.get(i).foodID == menuItemID){
				Menu.menu.get(i).price_per_person = price;
			}
		}
	}
	*/
}
