package edu.iit.cs445.delectable.tests;
import static org.junit.Assert.*;

import org.junit.Test;

import edu.iit.cs445.delectable.Address;

public class testAddress {

	Address testAddress = new Address("Greensburg", "PA", "15601", "18 Mechling Way");
	
//ACCESSOR TESTS
	@Test
	public void getCityTest() {
		assertEquals("Greensburg", testAddress.getCity());
	}
	@Test
	public void getStateTest() {
		assertEquals("PA", testAddress.getState());
	}
	@Test
	public void getZipTest() {
		assertEquals("15601", testAddress.getZip());
	}
	@Test
	public void getStreetTest() {
		assertEquals("18 Mechling Way", testAddress.getStreet());
	}
	
//MUTATOR TESTS
	@Test
	public void setCityTest(){
		testAddress.setCity("testCity");
		assertEquals("testCity", testAddress.getCity());
	}
	@Test
	public void setStateTest(){
		testAddress.setState("testState");
		assertEquals("testState", testAddress.getState());
	}
	@Test
	public void setZipTest(){
		testAddress.setZip("testZip");
		assertEquals("testZip", testAddress.getZip());
	}
	@Test
	public void setStreetTest(){
		testAddress.setStreet("testStreet");
		assertEquals("testStreet", testAddress.getStreet());
	}

//METHOD TESTS
	@Test
	public void equalsTest(){
		Address testAddress2 = new Address("Greensburg", "PA", "15601", "18 Mechling Way");
		Address testAddress3 = new Address("Chicago", "IL", "60616", "1926 S. Wells St.");
		assertTrue(testAddress.equals(testAddress2));
		assertFalse(testAddress.equals(testAddress3));
	}
}
