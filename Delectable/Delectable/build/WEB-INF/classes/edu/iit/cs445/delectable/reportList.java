package edu.iit.cs445.delectable;

import java.util.ArrayList;
import java.util.List;

public class reportList {

	public static List<Report> reportList = new ArrayList<Report>();

	public static void initiateReportList(){	
		reportList.clear();
		reportList.add(todayOrdersReport.todayOrdersReport);
		reportList.add(tomorrowOrdersReport.tomorrowOrdersReport);
		reportList.add(revenueReport.revenueReport);
		reportList.add(ordersDeliveryReport.ordersReport);
	}
	
	public static Report getReport(int reportId){
		for (int i = 0; i < reportList.size(); i++){
			Report testReport = reportList.get(i);
			if (reportId == testReport.reportID){
				testReport.initiateReport("", "");
				return testReport;
			}
		}
		
		return null;
	}
	
	public static Report getReportForDateRange(int reportId, String start_date, String end_date){
		Report testReport = getReport(reportId);
		if (testReport.name.equals("Orders to deliver today") || testReport.name.equals("Orders to deliver tomorrow")){
			testReport.initiateReport("", "");
			return testReport;
		} else {
			testReport.initiateReport(start_date, end_date);
			return testReport;
		}
	}
}
