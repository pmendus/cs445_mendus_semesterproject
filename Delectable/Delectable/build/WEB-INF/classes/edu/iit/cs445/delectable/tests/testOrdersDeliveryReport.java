package edu.iit.cs445.delectable.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import edu.iit.cs445.delectable.Address;
import edu.iit.cs445.delectable.Customer;
import edu.iit.cs445.delectable.ItemNotFoundException;
import edu.iit.cs445.delectable.Menu;
import edu.iit.cs445.delectable.Order;
import edu.iit.cs445.delectable.OrderTooSmallException;
import edu.iit.cs445.delectable.Report;
import edu.iit.cs445.delectable.foodCategory;
import edu.iit.cs445.delectable.menuItem;
import edu.iit.cs445.delectable.orderItem;
import edu.iit.cs445.delectable.orderList;
import edu.iit.cs445.delectable.ordersDeliveryReport;
import edu.iit.cs445.delectable.todayOrdersReport;
import edu.iit.cs445.delectable.tomorrowOrdersReport;

public class testOrdersDeliveryReport {

	@Test
	public void initiateReportTest() throws OrderTooSmallException, ItemNotFoundException{
		List<foodCategory> emptyCategory = new ArrayList<foodCategory>();
		menuItem testMenuItem = new menuItem("lasagna", 2.89, 3, emptyCategory);
		Menu.menu.add(testMenuItem);
		Address testAddress = new Address("testCity", "testState", "testZip", "testStreet");
		Customer testCustomer = new Customer("Patrick Mendus", "mendus59@gmail.com", "7249611117", testAddress);
		ArrayList<orderItem> testList = new ArrayList<orderItem>();
		int itemID = Menu.menu.get(0).foodID;
		orderItem testItem = new orderItem(itemID, 7);
		testList.add(testItem);
		Order testOrder = new Order(testCustomer, testList, "20160410", "");
		testOrder.status = "open";
		orderList.list.add(testOrder);
		Report testReport = new ordersDeliveryReport("ordersDeliveryReport");
		testReport.initiateReport("20160409", "20160411");
		assertTrue(ordersDeliveryReport.ordersReport.ordersDeliveryList.get(0).equals(testOrder));
	}

	@After
	public void tearDown(){
		Menu.menu.clear();
		orderList.list.clear();
		todayOrdersReport.todayOrdersReport.todayOrdersList.clear();
		tomorrowOrdersReport.tomorrowOrdersReport.tomorrowOrdersList.clear();
	}
}
