package edu.iit.cs445.delectable.tests;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import edu.iit.cs445.delectable.Menu;
import edu.iit.cs445.delectable.menuItem;

public class testMenu {

	Menu testMenu = new Menu();
	
	static menuItem lasagna = new menuItem("lasagna", 5, 2);
	static menuItem sushi = new menuItem("sushi", 10, 2);
	static menuItem salad = new menuItem("salad", 3, 2);
	static menuItem chicken = new menuItem("chicken", 14, 2);
	static menuItem coffee = new menuItem("coffee", 4, 2);
	
	@Test
	public void getEmptyMenuTest() {
		List<menuItem> testList = new ArrayList<menuItem>();
		Menu.menu = testList;
		assertTrue(testMenu.getMenu().isEmpty());
	}
	
	@Test
	public void addMenuItem() { //REQUIRES getOrder TO BE FUNCTIONAL
		testMenu.addMenuItem(lasagna);
		testMenu.addMenuItem(sushi);
		assertEquals("lasagna", testMenu.getMenu().get(0).name);
		assertEquals("sushi", testMenu.getMenu().get(1).name);
	}

	@After
	public void tearDown(){
		Menu.menu.clear();
	}
}