package edu.iit.cs445.delectable;
import java.util.List;
import java.util.ArrayList;

//Food creation is to be handled by the Business Manager's FrontEnd

public class menuItem{
	
	public int foodID;
	public String name;
	public double price_per_person;
	public int minimum_order;
	public List<foodCategory> categories;
	public String create_date;
	public String last_modified_date;
	
//CONSTRUCTORS
	public menuItem(){}
	
	public menuItem(String name, double price, int minimum_order, List<foodCategory> categories){
		this.foodID = UniqueIdGenerator.getUniqueID();
		this.name = name;
		this.price_per_person = price;
		this.minimum_order = minimum_order;
		this.categories = categories;
		this.create_date = dateHandler.getCurrentDate();
		this.last_modified_date = dateHandler.getCurrentDate();
	}
	
	//Set Categories later
	public menuItem(String name, double price, int minimum_order){
		this.foodID = UniqueIdGenerator.getUniqueID();
		this.name = name;
		this.price_per_person = price;
		this.minimum_order = minimum_order;
		this.categories = new ArrayList<foodCategory>();
		this.create_date = dateHandler.getCurrentDate();
		this.last_modified_date = dateHandler.getCurrentDate();
	}
	
//ACCESSORS
	public String getName(){
		return this.name;
	}
	public double getPrice(){
		return this.price_per_person;
	}
	public int getMinimumOrder(){
		return this.minimum_order;
	}
	public List<foodCategory> getCategories(){
		return this.categories;
	}
	
//MUTATORS
	public void setName(String name){
		this.name = name;
	}
	public void setPrice(double price){
		this.price_per_person = price;
	}
	public void setMinimumOrder(int minimumOrder){
		this.minimum_order = minimumOrder;
	}
	public void setCategories(List<foodCategory> categories){
		this.categories = categories;
	}
	
//METHODS
	public void addCategory(String name){
		if (name.equals("organic") || name.equals("gluten free") || name.equals("vegetarian") || name.equals("vegan") || name.equals("dairy free")){
			foodCategory newCat = new foodCategory(name);
			this.categories.add(newCat);
		}
	}
	public boolean equals(menuItem testFood){
		if (this.name.equals(testFood.getName()) && this.price_per_person == testFood.getPrice() && this.minimum_order == testFood.getMinimumOrder() && this.categories.equals(testFood.getCategories())){
			return true;
		} else {
			return false;
		}
	}
}
