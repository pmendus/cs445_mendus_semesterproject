package edu.iit.cs445.delectable;

public class Address {

	private String city;
	private String state;
	private String zip;
	private String street;

//CONSTRUCTORS
	public Address() {}
	
	public Address(String city, String state, String zip, String street){
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.street = street;
	}

//ACCESSORS
	public String getCity(){
		return city;
	}
	
	public String getState(){
		return state;
	}
	
	public String getZip(){
		return zip;
	}
	
	public String getStreet(){
		return street;
	}
	
//MUTATORS
	public void setCity(String city){
		this.city = city;
	}
	
	public void setState(String state){
		this.state = state;
	}
	
	public void setZip(String zip){
		this.zip = zip;
	}
	
	public void setStreet(String street){
		this.street = street;
	}
	
//METHODS
	public boolean equals(Address testAddress){
		if (this.city.equals(testAddress.getCity()) && this.state.equals(testAddress.getState()) && this.zip.equals(testAddress.getZip()) && this.street.equals(testAddress.getStreet())){
			return true;
		} else {
			return false;
		}
	}
}
