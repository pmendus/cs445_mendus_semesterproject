package edu.iit.cs445.delectable.Boundaries;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import edu.iit.cs445.delectable.Report;

import java.net.URI;

import javax.annotation.PostConstruct;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

//GET /report DONE
//GET /report/{rid}[?start_date=YYYYMMDD&end_date=YYYYMMDD]

@Path("/report")
public class ReportController {

	ReportBoundaryInterface rbi = new ReportBoundary();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllReports(){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonArray reportArray = rbi.getAllReports();
		String s = gson.toJson(reportArray);
		
		return Response.status(Response.Status.OK).entity(s).build();
	}
	
	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReport(@PathParam("id") int reportID, @QueryParam("start_date") String start_date, @QueryParam("end_date") String end_date){
		Report report;
		if (start_date == null || end_date == null){
			report = rbi.getReport(reportID);
		} else {
			report = rbi.getReport(reportID, start_date, end_date);
		}
		if (report == null){
			//return a 404
			return Response.status(Response.Status.NOT_FOUND).entity("Entity not found for ID: " + reportID).build();
		} else {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String output = gson.toJson(report);
			return Response.ok(output).build();
		}
	}
}
