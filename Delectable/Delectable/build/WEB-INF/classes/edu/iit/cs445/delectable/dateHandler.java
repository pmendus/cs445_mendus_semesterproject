package edu.iit.cs445.delectable;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Date;

public class dateHandler {

	public static String getCurrentDate(){
		Calendar cal = Calendar.getInstance(Locale.ENGLISH);		
		Date currentDate = cal.getTime();
		String dateString = formatDate(currentDate);
		
		return dateString;
	}
	
	public static String getTomorrowsDate(){
		Calendar cal = Calendar.getInstance(Locale.ENGLISH);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		Date tomorrow = cal.getTime();
		String dateString = formatDate(tomorrow);
		
		return dateString;
	}
	
	public static String formatDate(Date date){
		SimpleDateFormat sdfr = new SimpleDateFormat("yyyyMMdd");
		String dateString = sdfr.format(date);
		return dateString;
	}
}
