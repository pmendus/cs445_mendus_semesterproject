package edu.iit.cs445.delectable.Boundaries;

import java.util.List;

import com.google.gson.JsonArray;

import edu.iit.cs445.delectable.Report;

public interface ReportBoundaryInterface {

	JsonArray getAllReports();
	Report getReport(int reportID);
	Report getReport(int reportID, String start_date, String end_date);
	String getTodaysDate();
}
