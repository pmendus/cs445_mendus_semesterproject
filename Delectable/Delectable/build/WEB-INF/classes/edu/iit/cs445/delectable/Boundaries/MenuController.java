package edu.iit.cs445.delectable.Boundaries;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.iit.cs445.delectable.Menu;
import edu.iit.cs445.delectable.menuItem;

import java.util.ArrayList;
import java.util.List;

//import javax.annotation.PostConstruct;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

//GET /menu DONE
//GET /menu/{mid} DONE

@Path("/menu")
public class MenuController {

	private MenuBoundaryInterface mbi = new Menu();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllMenuItems(){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String userMenu = gson.toJson(mbi.getMenu());
		return Response.status(Response.Status.OK).entity(userMenu).build();
	}
	
	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMenuItem(@PathParam("id") int foodID){
		menuItem item = mbi.getMenuItem(foodID);
		if (item == null){
			//return a 404
			return Response.status(Response.Status.NOT_FOUND).entity("Entity not found for ID: " + foodID).build();
		} else {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String output = gson.toJson(item);
			return Response.ok(output).build();
		}
	}
}
