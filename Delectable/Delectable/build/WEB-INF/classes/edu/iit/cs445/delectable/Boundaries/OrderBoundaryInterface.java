package edu.iit.cs445.delectable.Boundaries;

import java.util.List;

import com.google.gson.JsonObject;

import edu.iit.cs445.delectable.ItemNotFoundException;
import edu.iit.cs445.delectable.Order;
import edu.iit.cs445.delectable.OrderTooSmallException;
import edu.iit.cs445.delectable.SameDayCancellationAttemptException;

public interface OrderBoundaryInterface {

	List<Order> getOrderList();
	Order getOrder(int orderID);
	List<Order> findOrderByDate(String date);
	void addOrder(Order order) throws OrderTooSmallException, ItemNotFoundException;
	JsonObject buildOrderJson(Order order);
	void cancelOrder(int orderID) throws SameDayCancellationAttemptException;
}
