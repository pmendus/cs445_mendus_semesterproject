package edu.iit.cs445.delectable.tests;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.iit.cs445.delectable.Address;
import edu.iit.cs445.delectable.Customer;
import edu.iit.cs445.delectable.ItemNotFoundException;
import edu.iit.cs445.delectable.Menu;
import edu.iit.cs445.delectable.Order;
import edu.iit.cs445.delectable.OrderTooSmallException;
import edu.iit.cs445.delectable.menuItem;
import edu.iit.cs445.delectable.orderItem;
import edu.iit.cs445.delectable.orderList;
import edu.iit.cs445.delectable.todayOrdersReport;
import edu.iit.cs445.delectable.tomorrowOrdersReport;

public class testOrderList {

	static menuItem lasagna = new menuItem("lasagna", 5.0, 2);
	static Address testAddress = new Address();
	static Customer testCustomer1 = new Customer("Patrick Mendus", "mendus59@gmail.com", "7244541217", testAddress);
	Order testOrder1 = new Order(testCustomer1, "Deliver to front");
	String deliveryDate = "20160420";
	
	@Before
	public void before() throws ItemNotFoundException, OrderTooSmallException{
		Menu.menu.add(lasagna);
		testOrder1.delivery_date = deliveryDate;
	}
	
	@Test
	public void testAddOrder() throws ItemNotFoundException, OrderTooSmallException{
		orderList.DelectableOrders.addOrder(testOrder1);
		assertEquals("Deliver to front", orderList.list.get(0).note);
	}

	@Test
	public void testValidateOrder() throws ItemNotFoundException, OrderTooSmallException{
		int itemID = lasagna.foodID;
		orderItem item = new orderItem(itemID, 8);
		testOrder1.addOrderItem(item);
		assertTrue(orderList.validateOrder(testOrder1));
	}

	@Test
	public void testFindOrderByDate() throws ItemNotFoundException, OrderTooSmallException{
		orderList.DelectableOrders.addOrder(testOrder1);
		List<Order> foundOrders = orderList.findOrderByDate(deliveryDate);
		assertTrue(testOrder1.orderID == foundOrders.get(0).orderID);
	}

	@Test
	public void testFindOrdersInDateRange() throws ItemNotFoundException, OrderTooSmallException{
		orderList.DelectableOrders.addOrder(testOrder1);
		List<Order> foundOrders = orderList.findOrdersInDateRange("20160419", "20160421");
		assertTrue(testOrder1.orderID == foundOrders.get(0).orderID);
	}

	@After
	public void tearDown(){
		Menu.menu.clear();
		orderList.list.clear();
		todayOrdersReport.todayOrdersReport.todayOrdersList.clear();
		tomorrowOrdersReport.tomorrowOrdersReport.tomorrowOrdersList.clear();
	}
}
