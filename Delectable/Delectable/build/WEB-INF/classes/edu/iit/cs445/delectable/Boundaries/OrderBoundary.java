package edu.iit.cs445.delectable.Boundaries;

import java.util.List;

import com.google.gson.JsonObject;

import edu.iit.cs445.delectable.ItemNotFoundException;
import edu.iit.cs445.delectable.Order;
import edu.iit.cs445.delectable.OrderTooSmallException;
import edu.iit.cs445.delectable.SameDayCancellationAttemptException;
import edu.iit.cs445.delectable.dateHandler;
import edu.iit.cs445.delectable.orderList;

public class OrderBoundary implements OrderBoundaryInterface{

	public List<Order> getOrderList(){
		return orderList.list;
	}
	
	public Order getOrder(int orderID){
		Order returnOrder = orderList.DelectableOrders.getOrder(orderID);
		return returnOrder;
	}
	
	public List<Order> findOrderByDate(String date){
		List<Order> returnList = orderList.findOrderByDate(date);
		return returnList;
	}
	
	public void addOrder(Order order) throws OrderTooSmallException, ItemNotFoundException{
		order.personal_info.setID();
		orderList.DelectableOrders.addOrder(order);
	}
	
	public JsonObject buildOrderJson(Order order){
		JsonObject orderObject = new JsonObject();
		orderObject.addProperty("id", order.orderID);
		orderObject.addProperty("cancel_url", "/order/cancel/"+order.orderID);
		return orderObject;
	}
	
	public void cancelOrder(int orderID) throws SameDayCancellationAttemptException{
		
		for (int i = 0; i < orderList.list.size(); i++){
			Order testOrder = orderList.list.get(i);
			if (testOrder.orderID == orderID){
				if (orderList.list.get(i).delivery_date.equals(dateHandler.getCurrentDate())){
					throw new SameDayCancellationAttemptException();
				} else {
					orderList.list.get(i).status = "cancelled";
				}
			}
		}
	}
}
