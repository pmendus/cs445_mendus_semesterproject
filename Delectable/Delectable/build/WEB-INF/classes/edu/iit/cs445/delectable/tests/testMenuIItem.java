package edu.iit.cs445.delectable.tests;
import static org.junit.Assert.*;

import java.util.List;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import edu.iit.cs445.delectable.foodCategory;
import edu.iit.cs445.delectable.menuItem;

public class testMenuIItem {

	menuItem testFood;
	
	List<foodCategory> catList = new ArrayList<foodCategory>();
	foodCategory vegan = new foodCategory("vegan");
	menuItem testFood2 = new menuItem("Lasagna", 2.49, 6);

	@Before
	public void init(){
		catList.add(vegan);
		testFood =  new menuItem("Lasagna", 2.49, 6, catList);
	}
//ACCESSOR TESTS
	@Test
	public void getNameTest() {
		assertEquals("Lasagna", testFood.getName());
	}
	@Test
	public void getPriceTest() {
		assertEquals(2.49, testFood.getPrice(), 0);
	}
	@Test
	public void getMinimumOrderTest(){
		assertEquals(6, testFood.getMinimumOrder());
	}
	@Test
	public void getCategoriesTest(){
		assertTrue(catList.equals(testFood.getCategories()));
	}

//MUTATOR TESTS
	@Test
	public void setNameTest(){
		testFood.setName("House Special");
		assertEquals("House Special", testFood.getName());
	}
	@Test
	public void setPriceTest(){
		testFood.setPrice(38.49);
		assertEquals(38.49, testFood.getPrice(), 0);
	}
	@Test
	public void setMinimumOrderTest(){
		testFood.setMinimumOrder(7);
		assertEquals(7, testFood.getMinimumOrder());
	}
	@Test
	public void setCategoriesTest(){
		testFood2.setCategories(catList);
		assertTrue(catList.equals(testFood2.getCategories()));
	}

//METHOD TESTS
	@Test
	public void equalsTest(){
		menuItem testFood3 = new menuItem("Lasagna", 2.49, 6, catList);
		menuItem testFood4 = new menuItem("House Special", 3.89, 7, catList);
		assertTrue(testFood3.equals(testFood));
		assertFalse(testFood4.equals(testFood));
	}
	@Test
	public void equalsTestWithEmptyCatList(){
		menuItem testFood3 = new menuItem("Lasagna", 2.49, 6);
		menuItem testFood4 = new menuItem("House Special", 3.89, 7);
		assertTrue(testFood3.equals(testFood2));
		assertFalse(testFood4.equals(testFood2));
	}
	@Test
	public void addValidCategoryTest(){
		foodCategory organic = new foodCategory("organic");
		catList.add(organic);
		testFood.addCategory("organic");
		assertTrue(catList.equals(testFood.getCategories()));
	}
	@Test
	public void addInvalidCategoryTest(){
		testFood.addCategory("contains nuts");
		assertTrue(catList.equals(testFood.getCategories()));
	}
}
