package edu.iit.cs445.delectable;

import java.util.ArrayList;
import java.util.List;

public class tomorrowOrdersReport extends Report{

	public static tomorrowOrdersReport tomorrowOrdersReport = new tomorrowOrdersReport("Orders to deliver tomorrow");
	public List<Order> tomorrowOrdersList = new ArrayList<Order>();
	
	public tomorrowOrdersReport(String name){
		this.name = name;
		this.reportID = UniqueIdGenerator.getUniqueID();
	}
	
	public void initiateReport(String start_date, String end_date){
		initiateReport();
	}
	public void initiateReport(){
		String dateString = dateHandler.getTomorrowsDate();
		List<Order> tomorrowList = orderList.findOrderByDate(dateString);
		tomorrowOrdersReport.tomorrowOrdersList = tomorrowList;
	}
}
