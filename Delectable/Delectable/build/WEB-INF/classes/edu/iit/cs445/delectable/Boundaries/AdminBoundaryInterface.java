package edu.iit.cs445.delectable.Boundaries;

import com.google.gson.JsonObject;

import edu.iit.cs445.delectable.Order;
import edu.iit.cs445.delectable.menuItem;
import edu.iit.cs445.delectable.surchargeHandler;

public interface AdminBoundaryInterface {

	surchargeHandler getSurchargeHandler();
	void setSurcharge(double surcharge);
	void createMenuItem(menuItem newItem);
	JsonObject putMenuItemResponse(menuItem item);
	void deliverOrder(int orderID);
	void setMenuItemPrice(int menuItemID, double price);
	Order getOrder(int orderID);

}
