package edu.iit.cs445.delectable;

public class OrderTooSmallException extends Exception{
	
	private double count;
	public OrderTooSmallException(double count){
		this.count = count;
	}
	
	public double getCount(){
		return this.count;
	}

}
