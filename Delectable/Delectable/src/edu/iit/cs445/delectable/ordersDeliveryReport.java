package edu.iit.cs445.delectable;

import java.util.ArrayList;
import java.util.List;

public class ordersDeliveryReport extends Report{

	public static ordersDeliveryReport ordersReport = new ordersDeliveryReport("Orders delivery report");
	public List<Order> ordersDeliveryList = new ArrayList<Order>();
	
	public ordersDeliveryReport(String name){
		this.name = name;
		this.reportID = UniqueIdGenerator.getUniqueID();
	}
	
	public void initiateReport(String start_date, String end_date){
		List<Order> ordersDeliveryList = orderList.findOrdersInDateRange(start_date, end_date);
		ordersReport.ordersDeliveryList = ordersDeliveryList;
	}
}
