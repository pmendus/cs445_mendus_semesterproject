package edu.iit.cs445.delectable;

import java.util.ArrayList;
import java.util.List;

public class todayOrdersReport extends Report{

	public static todayOrdersReport todayOrdersReport = new todayOrdersReport("Orders to deliver today");
	public List<Order> todayOrdersList = new ArrayList<Order>();
	
	public todayOrdersReport(String name){
		this.name = name;
		this.reportID = UniqueIdGenerator.getUniqueID();
	}
	
	public void initiateReport(String start_date, String end_date){
		initiateReport();
	}
	
	public void initiateReport(){
		String dateString = dateHandler.getCurrentDate();
		List<Order> todayList = orderList.findOrderByDate(dateString);
		todayOrdersReport.todayOrdersList = todayList;
	}
	
	
}
