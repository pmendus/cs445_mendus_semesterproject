package edu.iit.cs445.delectable.Boundaries;

import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import edu.iit.cs445.delectable.Report;
import edu.iit.cs445.delectable.dateHandler;
import edu.iit.cs445.delectable.reportList;

public class ReportBoundary implements ReportBoundaryInterface{

	public JsonArray getAllReports(){
		JsonArray reportArray = new JsonArray();
		reportList.initiateReportList();
		for (int i = 0; i < reportList.reportList.size(); i++){
			JsonObject reportObject = new JsonObject();
			Report tempReport = reportList.reportList.get(i);
			reportObject.addProperty("id", tempReport.reportID);
			reportObject.addProperty("name", tempReport.name);
			reportArray.add(reportObject);
		}
		return reportArray;
	}
	
	public Report getReport(int reportID){
		reportList.initiateReportList();
		return reportList.getReport(reportID);
	}
	public Report getReport(int reportID, String start_date, String end_date){
		reportList.initiateReportList();
		Report report = reportList.getReport(reportID);
		report.initiateReport(start_date, end_date);
		return report;
	}
	
	public String getTodaysDate(){
		return dateHandler.getCurrentDate();
	}
}
