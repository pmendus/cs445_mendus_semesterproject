package edu.iit.cs445.delectable.Boundaries;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import edu.iit.cs445.delectable.Customer;
import edu.iit.cs445.delectable.Order;
import edu.iit.cs445.delectable.orderList;

public class CustomerBoundary implements CustomerBoundaryInterface{
	
	public List<Customer> getAllCustomers(){
		List<Customer> customerList = new ArrayList<Customer>();
		for (int i = 0; i < orderList.list.size(); i++){
			Customer testCustomer = orderList.list.get(i).personal_info;
			boolean testBool = false;
			for (int k = 0; k < customerList.size(); k++){
				if (testCustomer.getID() == customerList.get(k).getID()){
					testBool = true;
				}
			}
			if (!testBool){
				customerList.add(testCustomer);
			}
		}
		return customerList;
	}
	
	public List<Customer> searchForCustomer(String query_string){
		List<Customer> searchList = new ArrayList<Customer>();
		CharSequence searchChars = query_string;
		for (int i = 0; i < orderList.list.size(); i++){
			Customer testCustomer = orderList.list.get(i).personal_info;
			if (testCustomer.getEmail().contains(searchChars) || testCustomer.getName().contains(searchChars) || testCustomer.getPhoneNumber().contains(searchChars)){
				searchList.add(testCustomer);
			}
		}
		return searchList;
	}
	
	public Customer getCustomerByID(int customerID){
		Customer foundCustomer = orderList.searchForCustomer(customerID);
		return foundCustomer;
	}
	
	public List<Order> getCustomerOrders(int customerID){
		List<Order> foundOrders = new ArrayList<Order>();
		for (int i = 0; i < orderList.list.size(); i++){
			Order testOrder = orderList.list.get(i);
			if (customerID == testOrder.personal_info.getID()){
				foundOrders.add(testOrder);
			}
		}
		return foundOrders;
	}
	
	public JsonObject getCustomerJsonObject(Customer customer, List<Order> orders){
		JsonObject customerJson = new JsonObject();
		customerJson.addProperty("id", customer.getID());
		customerJson.addProperty("name", customer.getName());
		customerJson.addProperty("email", customer.getEmail());
		customerJson.addProperty("phone", customer.getPhoneNumber());
		JsonArray orderArray = new JsonArray();
		for (int i = 0; i < orders.size(); i++){
			JsonObject orderObject = new JsonObject();
			Order addOrder = orders.get(i);
			orderObject.addProperty("id", addOrder.orderID);
			orderObject.addProperty("order_date", addOrder.order_date);
			orderObject.addProperty("delivery_date", addOrder.delivery_date);
			orderObject.addProperty("amount", addOrder.amount);
			orderObject.addProperty("surcharge", addOrder.surcharge);
			orderObject.addProperty("status", addOrder.status);
			orderArray.add(orderObject);
		}
		customerJson.add("orders", orderArray);
		return customerJson;
	}

}
