package edu.iit.cs445.delectable.Boundaries;

import java.util.List;

import com.google.gson.JsonObject;

import edu.iit.cs445.delectable.Customer;
import edu.iit.cs445.delectable.Order;

public interface CustomerBoundaryInterface {

	List<Customer> getAllCustomers();
	List<Customer> searchForCustomer(String query_string);
	Customer getCustomerByID(int customerID);
	List<Order> getCustomerOrders(int customerID);
	JsonObject getCustomerJsonObject(Customer customer, List<Order> orders);
}
