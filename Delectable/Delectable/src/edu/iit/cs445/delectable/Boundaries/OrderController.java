package edu.iit.cs445.delectable.Boundaries;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.iit.cs445.delectable.ItemNotFoundException;
import edu.iit.cs445.delectable.Order;
import edu.iit.cs445.delectable.OrderTooSmallException;
import edu.iit.cs445.delectable.SameDayCancellationAttemptException;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

//import javax.annotation.PostConstruct;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

//GET /order DONE
//GET /order[?date=YYYMMDD] DONE
//PUT /order DONE
//GET /order/{oid} DONE
//POST /order/cancel/{oid} DONE

@Path("/order")
public class OrderController {

	private OrderBoundaryInterface obi = new OrderBoundary();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllOrders(@QueryParam("date") String searchDate){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String userOrderList = "";
		if (searchDate == null){
			userOrderList = gson.toJson(obi.getOrderList());
		} else {
			userOrderList = gson.toJson(obi.findOrderByDate(searchDate));
		}
		return Response.status(Response.Status.OK).entity(userOrderList).build();
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
    public Response createOrder(@Context UriInfo uriInfo, String json) throws OrderTooSmallException, ItemNotFoundException{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Order order = gson.fromJson(json, Order.class);
        try{
        	obi.addOrder(order);
        } catch (OrderTooSmallException e){
        	return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Order size is too small").build();
        } catch (ItemNotFoundException e){
        	return Response.status(Response.Status.NOT_FOUND).entity("Menu item not found").build();
        }
        int id = order.orderID;
        String s = gson.toJson(obi.buildOrderJson(order));
        URI createdURIResource = URI.create(String.valueOf(id));
        return Response.created(createdURIResource).entity(s).build();
    }
	
	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrder(@PathParam("id") int orderID){
		Order order = obi.getOrder(orderID);
		if (order == null){
			//return a 404
			return Response.status(Response.Status.NOT_FOUND).entity("Entity not found for ID: " + orderID).build();
		} else {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String output = gson.toJson(order);
			return Response.ok(output).build();
		}
	}
	
	@Path("/cancel/{oid}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response cancelOrder(@PathParam("oid") int orderID){
		try {
		obi.cancelOrder(orderID);
		} catch (SameDayCancellationAttemptException e){
        	return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Orders cannot be cancelled on the same day as delivery.").build();
		}
		return Response.status(Response.Status.NO_CONTENT).build();
	}
}
