package edu.iit.cs445.delectable.Boundaries;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import edu.iit.cs445.delectable.Menu;
import edu.iit.cs445.delectable.Order;
import edu.iit.cs445.delectable.menuItem;
import edu.iit.cs445.delectable.surchargeHandler;

import java.net.URI;

import javax.annotation.PostConstruct;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

//PUT /admin/menu DONE
//POST /admin/menu/{mid}
//GET /admin/surcharge DONE
//POST /admin/surcharge DONE
//POST /admin/delivery/{oid} DONE

@Path("/admin")
public class AdminController {
	
	AdminBoundaryInterface abi = new AdminBoundary();
	
	@Path("/surcharge")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSurcharge(){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String surcharge = gson.toJson(abi.getSurchargeHandler());
		return Response.status(Response.Status.OK).entity(surcharge).build();
	}
	
	//NOTE: GO THROUGH BOUNDARY
	@Path("/surcharge")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response setSurcharge(String json) {
        Gson gson = new Gson();
        surchargeHandler newSurcharge = gson.fromJson(json, surchargeHandler.class);
        newSurcharge.setStaticPublicSurcharge();
        return Response.status(Response.Status.NO_CONTENT).build();
    }
	
	@Path("/menu")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
    public Response createMenuItem(@Context UriInfo uriInfo, String json) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        menuItem item = gson.fromJson(json, menuItem.class);
        abi.createMenuItem(item);
        JsonObject responseObject = abi.putMenuItemResponse(item);
        String s = gson.toJson(responseObject);
        URI createdURIResource = URI.create(String.valueOf(item.foodID));
        
        return Response.created(createdURIResource).entity(s).build();
    }
	
	@Path("/delivery/{oid}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response deliverItem(@PathParam("oid") int orderID){
		Order foundOrder = abi.getOrder(orderID);
		if (foundOrder == null){
			//return a 404
			return Response.status(Response.Status.NOT_FOUND).entity("Entity not found for ID: " + orderID).build();
		} else {
			abi.deliverOrder(orderID);
			return Response.status(Response.Status.NO_CONTENT).build();
		}
	}
	
	@Path("/menu/{mid}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
    public Response controlLamp(@PathParam("mid") int mid, String json) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		menuItem item = gson.fromJson(json, menuItem.class);
		double price = item.price_per_person;
        abi.setMenuItemPrice(mid, price);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @PostConstruct
    public void postConstruct() {
         Menu.DelectableMenu = new Menu();
    }
}