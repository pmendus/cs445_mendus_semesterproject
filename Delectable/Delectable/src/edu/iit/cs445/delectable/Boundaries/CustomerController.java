package edu.iit.cs445.delectable.Boundaries;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import edu.iit.cs445.delectable.Customer;
import edu.iit.cs445.delectable.Order;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

//GET /customer DONE
//GET /customer[?key=query_string] DONE
//GET /customer/{cid} DONE

@Path("/customer")
public class CustomerController {

	CustomerBoundaryInterface cbi = new CustomerBoundary();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCustomers(@QueryParam("key") String query_string){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String customers;
		if (query_string == null){
			customers = gson.toJson(cbi.getAllCustomers());
		} else {
			customers = gson.toJson(cbi.searchForCustomer(query_string));
		}
		return Response.status(Response.Status.OK).entity(customers).build();
	}
	
	@Path("/{cid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerByID(@PathParam("cid") int customerID){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Customer customer = cbi.getCustomerByID(customerID);
		if (customer == null){
			return Response.status(Response.Status.NOT_FOUND).entity("Entity not found for ID: " + customerID).build();
		} else {
			List<Order> customerOrders = cbi.getCustomerOrders(customerID);
			JsonObject customerObject = cbi.getCustomerJsonObject(customer, customerOrders);
			String customerString = gson.toJson(customerObject);
			return Response.status(Response.Status.OK).entity(customerString).build();	
		}
	}
}
