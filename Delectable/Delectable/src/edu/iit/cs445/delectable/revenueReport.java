package edu.iit.cs445.delectable;

public class revenueReport extends Report{
	
	protected String start_date;
	protected String end_date;
	public int orders_placed;
	public int orders_cancelled;
	public int orders_open;
	public double food_revenue;
	public double surcharge_revenue;
	
	public static revenueReport revenueReport = new revenueReport();
	
	public revenueReport(){
		this.reportID = UniqueIdGenerator.getUniqueID();
		this.name = "Revenue report";
	}
	
	public void initiateReport(String start_date, String end_date){
		if (start_date.length() <= 0 || end_date.length() <= 0){
			this.start_date = "0";
			this.end_date = "0";
		} else {
			this.start_date = start_date;
			this.end_date = end_date;
		}
		int test_orders_placed = 0;
		int test_orders_cancelled = 0;
		int test_orders_open = 0;
		double test_food_revenue = 0;
		double test_surcharge_revenue = 0;
		
		int startDateInt = Integer.parseInt(this.start_date);
		int endDateInt = Integer.parseInt(this.end_date);
		for (int i = 0; i < orderList.list.size(); i++){
			Order testOrder = orderList.list.get(i);
			int testDateInt = Integer.parseInt(testOrder.delivery_date);
			if (testDateInt >= startDateInt && testDateInt <= endDateInt){
				test_orders_placed ++;
				if (testOrder.status.equals("open") || testOrder.status.equals("delivered")){
					test_orders_open++;
					test_food_revenue += testOrder.amount;
					test_surcharge_revenue += (testOrder.amount * testOrder.surcharge);
				} else if (testOrder.status.equals("cancelled")){
					test_orders_cancelled++;
				}
			}
		}
		
		this.orders_placed = test_orders_placed;
		this.orders_cancelled = test_orders_cancelled;
		this.orders_open = test_orders_open;
		this.food_revenue = test_food_revenue;
		this.surcharge_revenue = test_surcharge_revenue;
	}
}
