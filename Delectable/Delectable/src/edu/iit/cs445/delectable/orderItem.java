package edu.iit.cs445.delectable;

public class orderItem {

	protected int id = -1; //Menu Item ID
	protected String name;
	protected int count;

//CONSTRUCTORS
	public orderItem(){}
	
	public orderItem(int id, int count) throws OrderTooSmallException, ItemNotFoundException{
		for (int i = 0; i < Menu.menu.size(); i++){
			menuItem tempItem = Menu.menu.get(i);
			if (tempItem.foodID == id){
				this.id = id;
				this.name = tempItem.name;
				if (count >= tempItem.minimum_order){
					this.count = count;
				} else {
					throw new OrderTooSmallException(count);
				}
			}
		}
		if (this.id == -1){
			throw new ItemNotFoundException();
		}
	}

	public static boolean validateOrderItem(orderItem testItem) throws OrderTooSmallException{
		boolean testBoolean = false;
		int testID = testItem.id;
		int testCount = testItem.count;
		
		for (int i = 0; i < Menu.menu.size(); i++){
			menuItem tempItem = Menu.menu.get(i);
			if (tempItem.foodID == testID){
				testBoolean = true;
				if (testCount < tempItem.minimum_order){
					throw new OrderTooSmallException(testCount);
				}
			}
		}
		
		return testBoolean;
	}
}
