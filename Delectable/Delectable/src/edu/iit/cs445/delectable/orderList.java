package edu.iit.cs445.delectable;
import java.util.*;

public class orderList{

	public static List<Order> list = new ArrayList<Order>();
	
	public static orderList DelectableOrders = new orderList();
	
	public orderList(){}
	
	public void addOrder(Order order) throws OrderTooSmallException, ItemNotFoundException{
		boolean testBoolean = validateOrder(order);
		if (testBoolean == false){
			throw new ItemNotFoundException();
		}
		order.status = "open";
		order.order_date = dateHandler.getCurrentDate();
		order.orderID = UniqueIdGenerator.getUniqueID();
		order.surcharge = surchargeHandler.publicSurcharge;
		order.calcCosts();
		list.add(order);
	}
	
	public static boolean validateOrder(Order order) throws OrderTooSmallException{
		boolean testBoolean = true;
		for(int i = 0; i < order.order_detail.size(); i++){
			if (!orderItem.validateOrderItem(order.order_detail.get(i))){
				testBoolean = false;
			}
		}
		return testBoolean;
	}
	
	public Order getOrder(int orderID){
		for (int i = 0; i < list.size(); i++){
			Order testOrder = list.get(i);
			if (testOrder.orderID == orderID){
				return testOrder;
			}
		}
		
		return null;
	}
	
	public static List<Order> findOrderByDate(String date){
		List<Order> searchList = new ArrayList<Order>();
		if (date == null){
			return searchList;
		}
		for (int i = 0; i < orderList.list.size(); i++){
			Order testOrder = orderList.list.get(i);
			if (testOrder.delivery_date.equals(date)){
				searchList.add(testOrder);
			}
		}
		return searchList;
	}

	/*
	public static List<Order> findPastOrders(){
		String date = dateHandler.getCurrentDate();
		int dateInt = Integer.parseInt(date);
		List<Order> searchList = new ArrayList<Order>();
		for(int i = 0; i < orderList.list.size(); i++){
			Order testOrder = orderList.list.get(i);
			int testOrderDateInt = Integer.parseInt(testOrder.delivery_date);
			if (testOrderDateInt < dateInt){
				searchList.add(testOrder);
			}
		}
		return searchList;
	}
	*/
	
	public static List<Order> findOrdersInDateRange(String start_date, String end_date){
		List<Order> searchList = new ArrayList<Order>();
		if (start_date.length() <= 0 || end_date.length() <= 0){
			return searchList;
		}
		int startDateInt = Integer.parseInt(start_date);
		int endDateInt = Integer.parseInt(end_date);
		for(int i = 0; i < orderList.list.size(); i++){
			Order testOrder = orderList.list.get(i);
			int testOrderDateInt = Integer.parseInt(testOrder.delivery_date);
			if (testOrderDateInt >= startDateInt && testOrderDateInt <= endDateInt){
				searchList.add(testOrder);
			}
		}
		return searchList;
	}
	
	public static Customer searchForCustomer(int customerID){
		Customer returnCustomer = null;
		for (int i = 0; i < list.size(); i++){
			if(list.get(i).personal_info.getID() == customerID){
				returnCustomer = list.get(i).personal_info;
				return returnCustomer;
			}
		}
		return returnCustomer;
	}
	
}
