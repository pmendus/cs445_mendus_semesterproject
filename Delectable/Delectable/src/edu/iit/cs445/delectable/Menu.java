package edu.iit.cs445.delectable;
import java.util.*;

import edu.iit.cs445.delectable.Boundaries.MenuBoundaryInterface;

public class Menu implements MenuBoundaryInterface{

	public static List<menuItem> menu = new ArrayList<menuItem>();
	
	public static Menu DelectableMenu;
	
	public Menu(){}

	public void addMenuItem(menuItem item){
		Menu.menu.add(item);
	}
	
	public menuItem getMenuItem(int itemID){
		for (int i = 0; i < menu.size(); i++){
			menuItem testItem = menu.get(i);
			if (testItem.foodID == itemID){
				return testItem;
			}
		}
		
		return null;
	}
	
	public List<menuItem> getMenu(){
		return Menu.menu;
	}
	
//ADD SEARCH METHOD
	
//ADD DATE METHODS
	
}