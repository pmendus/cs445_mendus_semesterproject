package edu.iit.cs445.delectable;
import java.util.ArrayList;

public class Order {

	public int orderID;
	public double amount;
	public double surcharge;
	public String status;
	public String order_date;
	public String delivery_date;
	public Customer personal_info;
	public String note;
	public ArrayList<orderItem> order_detail;

	
//CONSTRUCTORS
	public Order() {}
	
	//Create with a pre-created ArrayList
	public Order (Customer orderCustomer, ArrayList<orderItem> orderItems, String dateOrderDue, String specialInstructions){
		this.personal_info = orderCustomer;
		this.order_detail = orderItems;
		this.note = specialInstructions;
		this.delivery_date = dateOrderDue;
		this.orderID = UniqueIdGenerator.getUniqueID();
		
		this.surcharge = surchargeHandler.getSurcharge();
		this.status = "open";
		order_date = dateHandler.getCurrentDate();
		this.personal_info.setID();
	}
	
	//Create with intent of adding orders using the addOrderItem method
	public Order (Customer orderCustomer, String specialInstructions){
		this.personal_info = orderCustomer;
		this.note = specialInstructions;
		this.orderID = UniqueIdGenerator.getUniqueID();
		
		order_detail = new ArrayList<orderItem>();
		this.surcharge = surchargeHandler.getSurcharge();
		this.personal_info.setID();
		this.status = "open";
	}
		
//METHODS
	public void addOrderItem(orderItem item){
		this.order_detail.add(item);
	}

	protected double calcCosts(){
		double amount = 0; 
		for (int i = 0; i < order_detail.size(); i++){
			orderItem testItem = order_detail.get(i);
			int count = testItem.count;
			menuItem menuItem = Menu.DelectableMenu.getMenuItem(testItem.id);
			double price = menuItem.getPrice();
			amount += (count * price);
		}
		this.amount = amount;
		return amount;
	}
}
