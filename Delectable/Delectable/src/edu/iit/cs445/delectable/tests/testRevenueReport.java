package edu.iit.cs445.delectable.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import edu.iit.cs445.delectable.Customer;
import edu.iit.cs445.delectable.ItemNotFoundException;
import edu.iit.cs445.delectable.Menu;
import edu.iit.cs445.delectable.Order;
import edu.iit.cs445.delectable.OrderTooSmallException;
import edu.iit.cs445.delectable.Report;
import edu.iit.cs445.delectable.foodCategory;
import edu.iit.cs445.delectable.menuItem;
import edu.iit.cs445.delectable.orderItem;
import edu.iit.cs445.delectable.orderList;
import edu.iit.cs445.delectable.revenueReport;

public class testRevenueReport {
	
	@Test
	public void emptySearchStringTest() throws ItemNotFoundException, OrderTooSmallException{
		List<foodCategory> emptyCategory = new ArrayList<foodCategory>();
		menuItem testMenuItem = new menuItem("lasagna", 2.89, 3, emptyCategory);
		Menu.menu.add(testMenuItem);
		Customer testCustomer = new Customer();
		ArrayList<orderItem> testList = new ArrayList<orderItem>();
		int itemID = Menu.menu.get(0).foodID;
		orderItem testItem = new orderItem(itemID, 7);
		testList.add(testItem);
		Order testOrder = new Order(testCustomer, testList, "20160410", "");
		orderList.list.add(testOrder);
		Report testReport = new revenueReport();
		testReport.initiateReport("", "");
		assertTrue(revenueReport.revenueReport.orders_placed == 0);
		revenueReport.revenueReport = new revenueReport();
	}

	@After
	public void tearDown(){
		Menu.menu.clear();
		orderList.list.clear();
		revenueReport.revenueReport = new revenueReport();
	}
}
