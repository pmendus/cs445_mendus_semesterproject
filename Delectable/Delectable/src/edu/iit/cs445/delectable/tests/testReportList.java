package edu.iit.cs445.delectable.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

import edu.iit.cs445.delectable.Menu;
import edu.iit.cs445.delectable.Report;
import edu.iit.cs445.delectable.orderList;
import edu.iit.cs445.delectable.reportList;
import edu.iit.cs445.delectable.todayOrdersReport;
import edu.iit.cs445.delectable.tomorrowOrdersReport;

public class testReportList {

	@Test
	public void testInitiateReportList() {
		reportList.initiateReportList();
		assertTrue(reportList.reportList.size() == 4);
	}
	
	@Test
	public void testGetNullReport(){
		reportList.initiateReportList();
		Report testReport = reportList.getReport(1971);
		assertTrue(testReport == null);
	}

	@After
	public void tearDown(){
		Menu.menu.clear();
		orderList.list.clear();
		todayOrdersReport.todayOrdersReport.todayOrdersList.clear();
		tomorrowOrdersReport.tomorrowOrdersReport.tomorrowOrdersList.clear();
	}
}
