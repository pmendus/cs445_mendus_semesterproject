package edu.iit.cs445.delectable.tests;
import org.junit.After;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import edu.iit.cs445.delectable.Menu;
import edu.iit.cs445.delectable.orderList;
import edu.iit.cs445.delectable.todayOrdersReport;
import edu.iit.cs445.delectable.tomorrowOrdersReport;

@RunWith(Suite.class)
@SuiteClasses({testAddress.class, testCustomer.class, testMenuIItem.class, testMenu.class, testOrder.class,
	testOrderList.class, testTodayOrdersReport.class, testTomorrowOrdersReport.class, testOrdersDeliveryReport.class, 
	testRevenueReport.class, testReportList.class})
public class testDelectableSuite {
	@After
	public void tearDown(){
		Menu.menu.clear();
		orderList.list.clear();
		todayOrdersReport.todayOrdersReport.todayOrdersList.clear();
		tomorrowOrdersReport.tomorrowOrdersReport.tomorrowOrdersList.clear();
	}
}
