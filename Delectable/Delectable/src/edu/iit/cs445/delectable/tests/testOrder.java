package edu.iit.cs445.delectable.tests;
import static org.junit.Assert.*;

import org.junit.Test;

import edu.iit.cs445.delectable.Address;
import edu.iit.cs445.delectable.Customer;
import edu.iit.cs445.delectable.Order;
import edu.iit.cs445.delectable.menuItem;
import edu.iit.cs445.delectable.orderItem;

import java.util.ArrayList;


public class testOrder {

	static Address testAddress = new Address();
	static Customer testCustomer = new Customer("Patrick Mendus", "mendus59@gmail.com", "7244541217", testAddress);
	
	static menuItem lasagna = new menuItem("lasagna", 5, 2);
	static menuItem sushi = new menuItem("sushi", 10, 2);
	
	static orderItem lasagnaItem = new orderItem();
	static orderItem sushiItem = new orderItem();
	
	static ArrayList<orderItem> orderArrayList = new ArrayList<orderItem>();


//METHOD TESTS
	@Test
	public void addOrderItemTest(){
		Order order1 = new Order(testCustomer, "");
		order1.addOrderItem(lasagnaItem);
		assertTrue(order1.order_detail.contains(lasagnaItem));
	}
	
}
