#!/bin/bash
printf "\n\n\nAbout to install application, this may take a few minutes"
sleep 3

printf "\n\n\nInstalling JDK\n"
sudo apt-get update
sudo apt-get --assume-yes install default-jdk

printf "\n\n\nInstalling tomcat7\n"
sudo apt-get --assume-yes install tomcat7

printf "\n\n\nCompiling Java"
sudo cp ./resources/*.jar /usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext
sudo javac ./Delectable/build/WEB-INF/classes/edu/iit/cs445/delectable/*.java ./Delectable/build/WEB-INF/classes/edu/iit/cs445/delectable/Boundaries/*.java ./Delectable/build/WEB-INF/classes/edu/iit/cs445/delectable/tests/*.java

printf "\n\n\nBuilding delectable.war\n"
cd ./Delectable/build
jar -cvf Delectable.war *
printf "\n\n\nDeploying Delectable.war\n"
sudo cp Delectable.war /var/lib/tomcat7/webapps
printf "\n\n\nStopping server\n"
sudo service tomcat7 stop
printf "\n\n\nRestarting server\n"
sudo service tomcat7 start

printf "\n\n\n\nDelectable.war has been deployed please navigate to http://localhost:8080/Delectable\n"
