Programmer: Patrick Mendus
Project: Delectable Catering Management Software
Date: April, 2016

CONFIGURATION: Projects is expected to run on a clean build of Ubuntu 14.04 LTS.

RUNNING: From command line in this directory, execute the build script with super-user permissions (sudo sh build.sh). This command will install the required Java SDK and Tomcat and will compile and deploy the WAR webapp to the Tomcat server. From there, test using http://localhost:8080/Delectable.

COPYRIGHT: 
    Copyright (C)  2016  Patrick Mendus.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

BUGS: No known bugs as of 26 April 2016.

ACKNOWLEDGEMENTS: Credit to Jean-Rodney Larrieux for assisting with the build script.